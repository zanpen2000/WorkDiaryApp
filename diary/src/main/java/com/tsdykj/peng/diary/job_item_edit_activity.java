package com.tsdykj.peng.diary;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tsdykj.peng.diary.function.DiaryApplication;
import com.tsdykj.peng.diary.function.PathManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class job_item_edit_activity extends Activity {

    final static String TAG = "ItemEditACT";

    private EditText titleEdit;
    private EditText contentEdit;
    private CheckBox doneCheckbox;
    private String objectId;
    private String today;
    private SimpleDateFormat dateFormat;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diary_item_edit);

        titleEdit = (EditText) findViewById(R.id.edtItemName);
        contentEdit = (EditText) findViewById(R.id.edtItemContent);
        doneCheckbox = (CheckBox) findViewById(R.id.job_item_done);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        today = dateFormat.format(new Date());

        DiaryApplication.getInstance().addActivity(this);

        getData();
    }

    protected void getData() {
        Intent intent = getIntent();
        objectId = intent.getStringExtra("_id");
        titleEdit.setText(intent.getStringExtra("title"));
        contentEdit.setText(intent.getStringExtra("description"));

        String done = intent.getStringExtra("done_status");

        if (done != null && done != "" && done.equals("1")) {
            doneCheckbox.setChecked(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_diary_item_edit_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_job_item_save:
                progressDialog = ProgressDialog.show(this, getString(R.string.please_wait), getString(R.string.uploading_please_wait), false);
                saveData();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void validationDialog(String msg, final EditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dummy_content));
        builder.setMessage(msg);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editText.requestFocus();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean validationInput(){
        if (titleEdit.getText().length() == 0){
            validationDialog(getString(R.string.input_item_name_please), titleEdit);
            return false;
        }

        if (contentEdit.getText().length() == 0){
            validationDialog(getString(R.string.input_content_please), titleEdit);
            return false;
        }
        return true;
    }


    private void saveData() {

        if (!validationInput()) return;

        RequestQueue requestQueue = Volley.newRequestQueue(job_item_edit_activity.this);

        int method = Request.Method.POST;
        String url = PathManager.Host + "jobs";
        if (objectId != null && objectId.length() > 0) {
            url += "/" + objectId;
            method = Request.Method.PUT;
        }

        StringRequest stringRequest = new StringRequest(method,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                if (progressDialog != null) progressDialog.dismiss();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("", error.getMessage(), error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("title", titleEdit.getText().toString());
                map.put("description", contentEdit.getText().toString());
                //String done = doneCheckbox.isChecked() ? "1" : "0";
                //map.put("done_status", done);
                map.put("done_status", "");
                map.put("user_id", PathManager.CURRENT_USER_ID);
                map.put("date", today);
                return map;
            }
        };

        requestQueue.add(stringRequest);
    }
}
