package com.tsdykj.peng.diary;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.tsdykj.peng.diary.function.PathManager;

/**
 * Created by peng on 2015/4/23.
 */
public class about_webview_activity extends Activity{

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.about_webview);

        webView = (WebView)findViewById(R.id.webview_about);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(PathManager.Host + "about");
    }
}
