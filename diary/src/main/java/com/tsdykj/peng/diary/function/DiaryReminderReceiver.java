package com.tsdykj.peng.diary.function;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by peng on 2015/4/25.
 */
public class DiaryReminderReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(final Context context, Intent intent) {

        final String userid= intent.getStringExtra("user_id");

        if  (userid.equals("")){
            Log.d("DiaryReminderReceiver","Can not get CURRENT_USER_ID");
            return;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINESE);
        String today = dateFormat.format(new Date());

        String url = PathManager.Host + "excelfile/count/sent/" + userid+ "/" + today;

        Log.d("DiaryReminderReceiver",url);

        new VolleyStringRequestAsync(context, url, new RequestListener() {
            @Override
            public void ProcessData(String data) {

                 if (data.indexOf("0") > 0) {
                    PUtils.ShowNotification(context,userid);
                }
            }
        }).Execute();
    }
}
