package com.tsdykj.peng.diary.function;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;


/**
 * Created by peng on 2015/4/22.
 */
public class HttpConnectionTestTask extends AsyncTask<Void, Void, Boolean> {

    private GetDataListener listener;

    public HttpConnectionTestTask(GetDataListener li){this.listener = li;}

    @Override
    protected void onPostExecute(Boolean result) {
        listener.GetData(result);

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return isConnByHttp();
    }

    public boolean isConnByHttp()  {
        try {
            HttpGet httpGet = new HttpGet(PathManager.Host + "test");

            // set timeout
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);
            HttpClient client = new DefaultHttpClient(params);

            HttpResponse response = client.execute(httpGet);
            int ret = response.getStatusLine().getStatusCode();

            Log.d("HTTPTEST", ret + "");

            if (HttpStatus.SC_OK == ret){
                return true;
            }
            else{
                Log.d("HTTPTEST","false");
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
            //Log.d("HTTPTEST",e.getMessage());
            return false;
        }
    }
}
