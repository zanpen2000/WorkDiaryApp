package com.tsdykj.peng.diary.bean;

/**
 * Created by peng on 2015/4/22.
 */
public class UpdateInfo {
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
}
