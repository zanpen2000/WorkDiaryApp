package com.tsdykj.peng.diary.function;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peng on 2015/4/22.
 */
public class MailTask extends AsyncTask<String,Integer,Boolean> {

    private GetDataListener listener;

    public MailTask(GetDataListener li){
        this.listener = li;
    }

    @Override
    protected void onPostExecute(Boolean online) {
        listener.GetData(online);
    }

    @Override
    protected Boolean doInBackground(String... params) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(params[0]);
        String today = params[1];

        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("user_id", PathManager.CURRENT_USER_ID));
            nameValuePairs.add(new BasicNameValuePair("date", today));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);
            Log.d("MailTask", response.toString());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
