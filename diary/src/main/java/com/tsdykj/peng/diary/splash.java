package com.tsdykj.peng.diary;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.tsdykj.peng.diary.function.DiaryApplication;
import com.tsdykj.peng.diary.function.PathManager;


public class splash extends Activity {


    private static final int AUTO_HIDE_DELAY_MILLIS = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        SharedPreferences sp = getApplicationContext().getSharedPreferences("dyjob", MODE_PRIVATE);
        PathManager.CURRENT_USER_OBJECT_ID = sp.getString("user_object_id", "");
        PathManager.CURRENT_USER_ID = sp.getString("user_id", "");

        setContentView(R.layout.activity_splash);

        Thread thread = new Thread() {
            public void run() {
                try {
                    Thread.sleep(AUTO_HIDE_DELAY_MILLIS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                startActivity(new Intent(splash.this, MainActivity.class));
                finish();
            }
        };

        thread.start();
        DiaryApplication.getInstance().addActivity(this);
    }
}
