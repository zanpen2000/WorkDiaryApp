package com.tsdykj.peng.diary.function;

/**
 * Created by peng on 2015/4/24.
 */
public interface RequestListener {
    void ProcessData(String data);
}
