package com.tsdykj.peng.diary.function;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by peng on 2015/4/24.
 */
public class VolleyStringRequestAsync {

    private final String TAG = "VOLLEYSTRINGREQUESTASYNC";

    private final Context context;
    private final String url;
    private final RequestListener listener;

    public VolleyStringRequestAsync(Context context, String url, RequestListener li) {
        this.context = context;
        this.url = url;
        this.listener = li;
    }

    public void Execute() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.ProcessData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String msg = error.getMessage() == null || error.getMessage().equals("") ? error.toString() : error.getMessage();
                VolleyLog.d(TAG, "Error: " + msg);
                Toast.makeText(context,
                        msg,
                        Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(stringRequest);
    }

}
