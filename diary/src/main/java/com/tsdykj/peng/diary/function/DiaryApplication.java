package com.tsdykj.peng.diary.function;

import android.app.Activity;
import android.app.Application;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by peng on 2015/4/22.
 */
public class DiaryApplication extends Application {
    private List<Activity> activities = new LinkedList<>();
    private static DiaryApplication instance;

    private DiaryApplication(){}

    public static DiaryApplication getInstance(){
        if (null == instance){
            instance = new DiaryApplication();
        }
        return instance;
    }

    public void addActivity(Activity activity){
        activities.add(activity);
    }

    public void exit(){
        for (Activity activity:activities){
            activity.finish();
        }
        System.exit(0);
    }
}
