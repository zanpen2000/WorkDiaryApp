package com.tsdykj.peng.diary;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.tsdykj.peng.diary.function.DiaryApplication;
import com.tsdykj.peng.diary.function.GetDataListener;
import com.tsdykj.peng.diary.function.HttpConnectionTestTask;
import com.tsdykj.peng.diary.function.PathManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by peng on 2015/4/23.
 */
public class history_detail_activity extends Activity {

    private final static String TAG = "HISTORY_DETAIL_ACTIVITY";

    private ListView lstview;
    private List<Map<String, String>> jobList = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapter;
    private String fromDay;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestQueue = Volley.newRequestQueue(history_detail_activity.this);

        setContentView(R.layout.activity_history_detail);

        fromDay = getIntent().getStringExtra("date");

        setTitle(fromDay + " " + getString(R.string.job_diary));

        adapter = new SimpleAdapter(this, jobList, R.layout.diary_item, new String[]{
                "title", "description"
        }, new int[]{
                R.id._diary_item_name, R.id._diary_item_content
        });


        lstview = (ListView) findViewById(R.id._lv_item_list_history);
        lstview.setAdapter(adapter);

        DiaryApplication.getInstance().addActivity(this);
    }

    private void getData() {

        String url = PathManager.Host + "jobs/build/" + PathManager.CURRENT_USER_ID + "/" + fromDay;

        Log.d(TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());
                try {

                    jobList.clear();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject item = (JSONObject) response.get(i);

                        Map<String, String> map = new HashMap<String, String>();
                        map.put("_id", item.get("_id").toString());
                        map.put("title", item.getString("title"));
                        map.put("description", item.getString("description"));
                        map.put("done_status", item.getString("done_status"));
                        map.put("user_id", item.getString("user_id"));
                        map.put("date", item.getString("date"));
                        jobList.add(map);
                    }
                    Log.d(TAG, jobList.toString());
                    adapter.notifyDataSetChanged();
                    if (null != progressDialog)
                        progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog!=null) progressDialog.dismiss();
                String error_msg = (error.getMessage()==null || error.getMessage().equals(""))?error.toString():error.getMessage();
                VolleyLog.d(TAG, "Error: " +error_msg);
                Toast.makeText(getApplicationContext(),
                        error_msg, Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(jsonArrayRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();

        new HttpConnectionTestTask(new GetDataListener() {
            @Override
            public void GetData(boolean online) {
                if (online) {
                    getData();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(history_detail_activity.this);
                    builder.setMessage(getString(R.string.serveroffline));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    DiaryApplication.getInstance().exit();
                }
            }
        }).execute();
    }

}
